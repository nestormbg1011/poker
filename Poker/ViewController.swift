//
//  ViewController.swift
//  Poker
//
//  Created by Bootcamp 6 on 10/24/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var card1: UIImageView!
    @IBOutlet weak var card2: UIImageView!
    @IBOutlet weak var card3: UIImageView!
    @IBOutlet weak var card4: UIImageView!
    @IBOutlet weak var card5: UIImageView!
    @IBOutlet weak var jugarBtn: UIButton!
    @IBOutlet weak var resultado: UILabel!
    
    @IBAction func jugar(_ sender: UIButton) {
        let cartas = [card1, card2, card3, card4, card5]
        let juego = Poker()
        juego.crearMazo()
        juego.hacerJugada()
        let jugadaResultado = juego.identificarJugada()
        
        for i in 0...4 {
            let carta = juego.cartasJugada[i].getCarta()
            cartas[i]!.image = UIImage(imageLiteralResourceName: carta)
        }
        
        resultado.text = jugadaResultado
    }
    /*let cartas = [carta1, carta2, carta3, carta4, carta5]
    let juego = Poker()
    
    juego.crearMazo()
    juego.hacerJugada()
    
    for i in 0...4 {
        cartas[i]!.text = juego.cartasJugada[i].getCarta()
    }*/
}

