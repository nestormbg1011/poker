//
//  Carta.swift
//  Poker
//
//  Created by Bootcamp 6 on 10/24/22.
//

class Carta {
    var valor: String = ""
    var valorNum: Int = 0
    var palo: String = ""
    var numeroALetra = ["A": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7,
    "8": 8, "9": 9, "T": 10, "J": 11, "Q": 12, "K": 13]

    func setValor(valor: String) {
        self.valor = valor
        self.valorNum = numeroALetra[valor]!
    }

    func setPalo(palo: String) {
        self.palo = palo
    }

    func getValor() -> String {
        return self.valor
    }

    func getPalo() -> String {
        return self.palo
    }
    
    func getValorNum() -> Int {
        return self.valorNum
    }
    
    func getCarta() -> String {
        return self.valor + self.palo
    }
}
