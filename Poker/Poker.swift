//
//  Poker.swift
//  Poker
//
//  Created by Bootcamp 6 on 10/24/22.
//

class Poker {
    var valores = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"]
    var palos = ["S", "C", "H", "D"]
    var mazo = [String: Int]()
    var cartasJugada: Array<Carta> = []


    func crearMazo() {
        for valor in valores {
            for palo in palos {
                mazo[valor + palo] = 0
            }
        }
    }

    func hacerJugada() {
        for _ in 1...5 {
            var valorPos = Int.random(in: 0...12)
            var paloPos = Int.random(in: 0...3)

            while mazo[valores[valorPos] + palos[paloPos]] != 0 {
                valorPos = Int.random(in: 0...12)
                paloPos = Int.random(in: 0...3)
            }

            mazo[valores[valorPos] + palos[paloPos]] = 1

            let carta = Carta()
            carta.setValor(valor: valores[valorPos])
            carta.setPalo(palo: palos[paloPos])

            cartasJugada.append(carta)
        }
    }

    func identificarJugada() -> String {
        cartasJugada = self.cartasJugada.sorted { $0.valorNum > $1.valorNum }
        var secuencia = 0
        var resultado = ""
        // Escalera Color
        for i in 0...3 {
            if (cartasJugada[i].getValorNum() - cartasJugada[i + 1].getValorNum()) == 1 {
                secuencia += 1
            }
        }

        if secuencia == 4 && cartasJugada[0].getPalo() == cartasJugada[1].getPalo() && cartasJugada[0].getPalo() == cartasJugada[2].getPalo() && cartasJugada[0].getPalo() == cartasJugada[3].getPalo() && cartasJugada[0].getPalo() == cartasJugada[4].getPalo() {
            resultado = "Escalera Color"
            return resultado
        }
        // Poker
        if cartasJugada[0].getValorNum() == cartasJugada[1].getValorNum() && cartasJugada[0].getValorNum() == cartasJugada[2].getValorNum() && cartasJugada[0].getValorNum() == cartasJugada[3].getValorNum() && cartasJugada[0].getValorNum() == cartasJugada[4].getValorNum() {
            resultado = "Poker"
            return resultado
        }
        // Full
        // 5 5 5 3 3

        if cartasJugada[0].getValorNum() == cartasJugada[1].getValorNum() && cartasJugada[0].getValorNum() == cartasJugada[2].getValorNum() && cartasJugada[4].getValorNum() == cartasJugada[3].getValorNum() {
            resultado = "Full"
            return resultado
        }

        cartasJugada = self.cartasJugada.sorted { $0.valorNum < $1.valorNum }

        // 3 3 3 5 5

        if cartasJugada[0].getValorNum() == cartasJugada[1].getValorNum() && cartasJugada[0].getValorNum() == cartasJugada[2].getValorNum() && cartasJugada[4].getValorNum() == cartasJugada[3].getValorNum() {
            resultado = "Full"
            return resultado
        }
        // Color
        if cartasJugada[0].getPalo() == cartasJugada[1].getPalo() && cartasJugada[0].getPalo() == cartasJugada[2].getPalo() && cartasJugada[0].getPalo() == cartasJugada[3].getPalo() && cartasJugada[0].getPalo() == cartasJugada[4].getPalo() {
            resultado = "Color"
            return resultado
        }

        // Escalera
        secuencia = 0
        for i in 0...3 {
            if (cartasJugada[i].getValorNum() - cartasJugada[i + 1].getValorNum()) == 1 {
                secuencia += 1
            }
        }

        if secuencia == 4 {
            resultado = "Escalera"
            return resultado
        }
        return resultado
    }

    func printJugada() {
        for carta in cartasJugada {
            print(carta.getValor() + carta.getPalo(), carta.getValorNum())
        }
    }

    func printMazo() {
        print(mazo)
    }
}
